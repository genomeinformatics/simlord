Changelog
=========

Version 1.0.4 (2020-01-07)
---------------------------

Bugs fixed
~~~~~~~~~~
- Added missing else for parameter sam_output.


Other Changes
~~~~~~~~~~~~~~
- Changed read names.
-- New name format: 'm{read_number}/{read_length}/CCS read_information'
-- Added parameter --old-read-names for old read names where all information is encoded in one large string delimited by ';'.


Version 1.0.3 (2019-05-20)
---------------------------

New Features
~~~~~~~~~~~~
- Added new parameter --gzip to gzip the output reads fastq file.
- If "-" instead of a filename is given, the reads are printed to sdt-out.
- In this case without further specification the sam-file gets the name "reads.sam" in the current working directory.

Other Changes
~~~~~~~~~~~~~~
- Changed coverage parameter from int to float allowing fractional coverage values.
- Changed delimiter in read id from _ to ;
- Added chromosome name to read id
- Changed id of mate read in sam file to result in "*" instead of "=".
- Changed fastq writing from text to byte writing to speed up I/O


Version 1.0.2 (2017-03-17)
---------------------------

New Features
~~~~~~~~~~~~

- Draw chromosomes for reads weighted with their length instead of equal distributed. This leads to a equal distributed read coverage over the chromosomes. Previous behaviour with equal probabilities for each chromosome can be activated with parameter --uniform-chromosome-probability.

- Parameter --coverage: Determine number of reads depending on the desired read coverage of the whole reference genome.

- Parameter --without-ns: Sample the reads only from regions completly without Ns.

Warning: Using --without-ns may lead to biased read coverage depending on the size of contigs without Ns and the expected readlength.

Bugs fixed
~~~~~~~~~~

- CIGAR string had sometimes wrong count of last match because of false extension after deletion.


Version 1.0.1 (2017-01-03)
---------------------------

Bugs fixed
~~~~~~~~~~

- Removed nargs=1 at parameter --probability-threshold leading to an error when changing the parameter.


Version 1.0.0 (2016-07-13)
---------------------------


API Changes
~~~~~~~~~~~

- Changed SEQ in SAM file to reverse complemented read instead of the original read for reads mapping to the reverse complement of the reference.

Example:
::



    reference       ATCG     read   CAAT
    true alignment  ||X|
                    ATTG

    Before: SEQ CAAT and CIGAR string 2=1X1=
    Now:    SEQ ATTG and CIGAR string 2=1X1=


Version 0.7.4 (2016-06-15)
---------------------------

Bugs fixed
~~~~~~~~~~

- Set encoding for version reading explicitly.


Version 0.7.3 (2016-04-11)
---------------------------

Other Changes
~~~~~~~~~~~~~~
- Updated descriptions.
- Show version in help message.
- Added --version option.

